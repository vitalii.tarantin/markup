$(function () {
    $('.header__btn').on('click', function () {
        $('.rightside-menu').removeClass('rightside-menu__close')
    });
    $('.rightside-menu__close').on('click', function () {
        $('.rightside-menu').addClass('rightside-menu__close')
    });


    $('.header__btn-menu').on('click', function () {
        $('.menu').toggleClass('menu--open');
    });


    if ($(window).width() < 651) {
        $('.works-path__item--measuring').appendTo($('.works-path__item-box'));
    }

    $('.top__slider').slick({
        arrows: false,
        dots: true,
        fade: true,
        autoplay: true
    });

    $('.contact-slider').slick({
        slidesToShow: 10,
        slidesToScroll: 10,
        arrows: false,
        dots: true,
        responsive: [
            {
                breakpoint: 1700,
                settings: {
                    slidesToShow: 8,
                    slidesToScroll: 8,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 1510,
                settings: {
                    slidesToShow: 6,
                    slidesToScroll: 6
                }
            },
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4
                }
            },
            {
                breakpoint: 840,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 550,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 375,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
    ]
    });
    $('.article-slider__box').slick({
        prevArrow: '<button type="button" class="article-slider__arrow article-slider__arrowright"><img src="../images/next.svg" alt="next"></button>',
        nextArrow: '<button type="button" class="article-slider__arrow article-slider__arrowleft"><img src="../images/back.svg" alt="prev"></button>'
    });


    var mixer = mixitup('.gallery__inner', {
        load: {
            filter: '.living'
        }
    });
})
